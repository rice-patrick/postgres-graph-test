import java.sql.*;
import java.util.Properties;

public class QueryData {

    public static void main(String[] args ) throws Exception {

        Connection conn = getConnection();

        CallableStatement cs = conn.prepareCall( "WITH RECURSIVE search_graph( user_a, user_b, depth, path) AS ( SELECT g.user_a , g.user_b , 1 as depth, ARRAY[g.user_a] as path FROM relationships AS g WHERE g.user_a = 1 UNION ALL SELECT g.user_a , g.user_b , sg.depth + 1 as depth, path || g.user_a as path FROM relationships AS g, search_graph AS sg WHERE g.user_a = sg.user_b AND (g.user_a <> ALL(sg.path)) AND sg.depth <= 3 ) SELECT * FROM search_graph sg; ");

        ResultSet rs = cs.executeQuery();
        int i = 0;
        while(rs.next()) {

           i++;
           if(i % 1000 == 0)
               System.out.println(i);
        }
        System.out.print(i);

    }

    private static Connection getConnection() throws SQLException {
        String url = "jdbc:postgresql://localhost:5432/postgres";
        Properties props = new Properties();
        props.setProperty("user","postgres");
        props.setProperty("password", "mysecretpassword");
        //props.setProperty("ssl","true");
        return  DriverManager.getConnection(url, props);
    }
}
