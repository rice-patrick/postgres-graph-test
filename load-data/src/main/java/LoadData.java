import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;

import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Properties;

public class LoadData {

    /**
     * Loads all data from the soc-pokec files into a postgres database, and builds the database schema
     * as required.
     *
     * @param args
     */
    public static void main(String[] args) throws Exception {

        Connection conn = getConnection();
        setupDb(conn);

        Reader reader = new BufferedReader(
                new FileReader(
                        "soc-pokec-profiles.txt"));
        CSVReader profiles = buildCSVReader(reader);

        int inserted = 0;
        PreparedStatement ps = conn.prepareStatement("Insert into public.profiles (user_id) values (?)");
        String[] line = null;
        while((line = profiles.readNext()) != null) {
            ps.setInt(1 ,Integer.parseInt(line[0]));
            ps.addBatch();

            inserted++;
            if(inserted % 10000 == 0)
            {
                ps.executeBatch();
                System.out.println(String.format("Inserted %d into profiles", inserted));
            }
        }

        profiles.close();
        reader.close();

        reader = new BufferedReader(
                new FileReader(
                        "soc-pokec-relationships.txt"));
        profiles = buildCSVReader(reader);

        inserted = 0;
        ps = conn.prepareStatement("Insert into public.relationships (user_a, rel_type, user_b) values (?,?,?)");
        line = null;
        while((line = profiles.readNext()) != null) {
            ps.setInt(1 ,Integer.parseInt(line[0]));
            ps.setString(2, "Friend");
            ps.setInt(3, Integer.parseInt(line[1]));
            ps.addBatch();

            inserted++;
            if(inserted % 10000 == 0)
            {
                ps.executeBatch();
                System.out.println(String.format("Inserted %d into rel", inserted));
            }
        }
    }

    private static void setupDb(Connection conn) throws SQLException {
        conn.prepareStatement(
                "CREATE TABLE IF NOT EXISTS public.profiles (user_id integer PRIMARY KEY);")
                .execute();

        conn.prepareStatement(
                "CREATE TABLE IF NOT EXISTS public.relationships (rel_id INT GENERATED ALWAYS AS IDENTITY," +
                        " user_a integer," +
                        " rel_type varchar," +
                        " user_b integer);")
                .execute();

        conn.prepareStatement(
                "CREATE INDEX ON public.relationships (user_a, user_b)")
                .execute();
    }

    private static Connection getConnection() throws SQLException {
        String url = "jdbc:postgresql://localhost:5432/postgres";
        Properties props = new Properties();
        props.setProperty("user","postgres");
        props.setProperty("password", "mysecretpassword");
        //props.setProperty("ssl","true");
        return  DriverManager.getConnection(url, props);
    }

    private static CSVReader buildCSVReader(Reader input) {
        final CSVParser parser = new CSVParserBuilder()
                .withSeparator('\t')
                .withIgnoreQuotations(true)
                .build();

        return new CSVReaderBuilder(input)
                .withSkipLines(1)
                .withCSVParser(parser)
                .build();
    }

}
