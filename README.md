# Postgres Graph Test

This project exists to test out recursive CTEs used to build recursive graph queries in postgres.
In order for this project to work, you will need to download the soc-pokec files from the following location: 
https://snap.stanford.edu/data/soc-pokec.html
